import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { JwtService } from "@nestjs/jwt";
import { Observable } from "rxjs";
import { Role } from "src/role/role.entity";

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private jwtService: JwtService,
                private reflector: Reflector){}

    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        try {
            const requiredRoles = this.reflector.getAllAndOverride<string[]>('roles', [
                context.getHandler(),
                context.getClass()
            ])

            if(!requiredRoles) return true

            const req = context.switchToHttp().getRequest()
            const authHeader = req.headers.authorization
            const bearer = authHeader.split(' ')[0]
            const token = authHeader.split(' ')[1]

            if(bearer !== 'Bearer' || !token) throw new HttpException('User not authorized', HttpStatus.UNAUTHORIZED)

            const user = this.jwtService.verify(token)
            req.user = user

            return user.roles.some((role: Role) => requiredRoles.includes(role.value))
        } catch (error) {
            throw new HttpException('Access denied', HttpStatus.FORBIDDEN)
        }
    }
}