import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from 'src/user/dto/create-user.dto';
import { UserService } from 'src/user/user.service';
import * as bcrypt from 'bcryptjs';
import { User } from 'src/user/user.entity';

@Injectable()
export class AuthService {
    constructor(private userService: UserService,
                private jwtService: JwtService) {}

    async signin(dto: CreateUserDto): Promise<object>{
        const user = await this.validateUser(dto)
        return this.generateToken(user)
    }

    async signup(dto: CreateUserDto): Promise<object>{
        const candidate = await this.userService.getByLogin(dto.login)
        if(candidate) throw new HttpException('User with this login already exists', HttpStatus.BAD_REQUEST)
        
        const hashPassword = await bcrypt.hash(dto.password, 7)        
        const user = await this.userService.create({...dto, password: hashPassword})

        return this.generateToken(user)
    }

    private async generateToken(user: User): Promise<object>{
        const payload = {login: user.login, id: user.id, roles: user.roles}
        return {
            token: this.jwtService.sign(payload)
        }
    }

    private async validateUser(dto: CreateUserDto): Promise<User> {
        const user = await this.userService.getByLogin(dto.login)
        if(!user) throw new HttpException('User not found', HttpStatus.UNAUTHORIZED)

        const isValidPassword = await bcrypt.compare(dto.password, user.password)
        if(!isValidPassword) throw new HttpException('Invalid password', HttpStatus.UNAUTHORIZED)

        return user
    }
}
