import { Body, Controller, HttpStatus, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreateUserDto } from 'src/user/dto/create-user.dto';
import { AuthService } from './auth.service';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) {}

    @ApiOperation({summary: 'Sign in'})
    @ApiResponse({status: HttpStatus.OK, description: 'Ok', type: Object})
    @ApiResponse({status: HttpStatus.BAD_REQUEST, description: 'Bad request', type: null})
    @ApiResponse({status: HttpStatus.UNAUTHORIZED, description: 'Unauthorized', type: null})
    @Post('signin')
    @UsePipes(ValidationPipe)
    signin(@Body() dto: CreateUserDto): object{
        return this.authService.signin(dto)
    }

    @ApiOperation({summary: 'Sign up'})
    @ApiResponse({status: HttpStatus.OK, description: 'Ok', type: Object})
    @ApiResponse({status: HttpStatus.BAD_REQUEST, description: 'Bad request', type: null})
    @Post('signup')
    @UsePipes(ValidationPipe)
    signup(@Body() dto: CreateUserDto): object{
        return this.authService.signup(dto)
    }
}
