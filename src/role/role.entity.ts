import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { User } from "src/user/user.entity";
import { ApiProperty } from "@nestjs/swagger";

@Entity()
export class Role {
    @ApiProperty({example: '1', description: 'unique identificator'})
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty({example: 'ADMIN', description: 'role name'})
    @Column()
    value: string;

    @ApiProperty({example: 'access to admin panel', description: 'description of access to functions'})
    @Column()
    description: string;

    @ApiProperty({example: [User], description: 'array of users'})
    @ManyToMany(() => User, user => user.roles)
    users: User[]
}