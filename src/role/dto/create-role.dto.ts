import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, Length } from "class-validator";

export class CreateRoleDto {
    @ApiProperty({example: 'MODERATOR', description: 'role name'})
    @IsNotEmpty()
    @Length(4)
    readonly value: string;

    @ApiProperty({example: 'access to moderator panel', description: 'description of access to functions'})
    @IsNotEmpty()
    @Length(8, 256)
    readonly description: string;
}