import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateRoleDto } from './dto/create-role.dto';
import { Role } from './role.entity';

@Injectable()
export class RoleService {
    constructor(@InjectRepository(Role) private roleRepository: Repository<Role>) {}

    async create(dto: CreateRoleDto): Promise<Role>{
        return await this.roleRepository.save(dto)
    }

    async getAll(): Promise<Role[]>{
        return this.roleRepository.find()
    }

    async getByValue(value: string): Promise<Role>{
        const role = await this.roleRepository.findOneBy({value: value})

        if(!role) throw new HttpException(`Cant find role with value: ${value}`, HttpStatus.NOT_FOUND)

        return role
    }
}
