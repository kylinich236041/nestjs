import { Body, Controller, Get, HttpStatus, Param, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CustomHttpExceptionResponse } from 'src/core/models/http-exception-response.interface';
import { CreateRoleDto } from './dto/create-role.dto';
import { Role } from './role.entity';
import { RoleService } from './role.service';

@ApiTags('role')
@Controller('role')
export class RoleController {
    constructor(private roleService: RoleService) {}

    @ApiOperation({summary: 'Create role'})
    @ApiResponse({status: HttpStatus.OK, description: 'Ok', type: Role})
    @ApiResponse({status: HttpStatus.BAD_REQUEST, description: 'Bad request', type: CustomHttpExceptionResponse})
    @Post()
    @UsePipes(ValidationPipe)
    create(@Body() dto: CreateRoleDto): Promise<Role>{
        return this.roleService.create(dto)
    }

    @ApiOperation({summary: 'Get all roles'})
    @ApiResponse({status: HttpStatus.OK, description: 'Ok', type: [Role]})
    @Get()
    getAll(): Promise<Role[]>{
        return this.roleService.getAll()
    }

    @ApiOperation({summary: 'Get role by value'})
    @ApiResponse({status: HttpStatus.OK, description: 'Ok', type: Role})
    @ApiResponse({status: HttpStatus.NOT_FOUND, description: 'Not found', type: CustomHttpExceptionResponse})
    @Get(':value')
    getByValue(@Param('value') value: string): Promise<Role>{
        return this.roleService.getByValue(value)
    }
}
