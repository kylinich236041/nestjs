import { ApiProperty } from "@nestjs/swagger";
import { User } from "src/user/user.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Article {
    @ApiProperty({example: '1', description: 'unique identificator'})
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty({example: 'About NestJS part 1', description: 'title'})
    @Column()
    title: string;

    @ApiProperty({example: 'Introduction\nNest (NestJS) is a framework for building efficient, scalable Node.js server-side applications...', description: 'content'})
    @Column()
    content: string;

    //@ApiProperty({example: User, description: 'article owner'})
    @ManyToOne(() => User, user => user.articles, {onDelete: 'CASCADE'})
    user: User
}