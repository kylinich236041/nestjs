import { Body, Controller, Delete, Get, HttpStatus, Param, ParseIntPipe, Post, Put, Req, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Auth } from 'src/auth/auth.decorator';
import { CustomHttpExceptionResponse } from 'src/core/models/http-exception-response.interface';
import { Article } from './article.entity';
import { ArticleService } from './article.service';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';

@ApiTags('article')
@Controller('article')
export class ArticleController {
    constructor(private articleService: ArticleService) {}

    @ApiOperation({summary: 'Create article'})
    @ApiResponse({status: HttpStatus.OK, description: 'Ok', type: Article})
    @ApiResponse({status: HttpStatus.BAD_REQUEST, description: 'Bad request', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.UNAUTHORIZED, description: 'Unauthorized', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.FORBIDDEN, description: 'Forbidden', type: CustomHttpExceptionResponse})
    @Auth('USER')
    @Post()
    @UsePipes(ValidationPipe)
    create(@Body() dto: CreateArticleDto, @Req() req): Promise<Article>{
        return this.articleService.create(dto, req.user)
    }

    @ApiOperation({summary: 'Get all articles'})
    @ApiResponse({status: HttpStatus.OK, description: 'Ok', type: [Article]})
    @ApiResponse({status: HttpStatus.UNAUTHORIZED, description: 'Unauthorized', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.FORBIDDEN, description: 'Forbidden', type: CustomHttpExceptionResponse})
    @Auth('USER')
    @Get()
    getAll(): Promise<Article[]>{
        return this.articleService.getAll()
    }

    @ApiOperation({summary: 'Get article by id'})
    @ApiResponse({status: HttpStatus.OK, description: 'Ok', type: Article})
    @ApiResponse({status: HttpStatus.NOT_FOUND, description: 'Not found', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.UNAUTHORIZED, description: 'Unauthorized', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.FORBIDDEN, description: 'Forbidden', type: CustomHttpExceptionResponse})
    @Auth('USER')
    @Get(':id')
    getById(@Param('id', ParseIntPipe) id: number): Promise<Article>{
        return this.articleService.getById(id)
    }

    @ApiOperation({summary: 'Update article'})
    @ApiResponse({status: HttpStatus.OK, description: 'Ok', type: Article})
    @ApiResponse({status: HttpStatus.NOT_FOUND, description: 'Not found', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.BAD_REQUEST, description: 'Bad request', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.UNAUTHORIZED, description: 'Unauthorized', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.FORBIDDEN, description: 'Forbidden', type: CustomHttpExceptionResponse})
    @Auth('USER')
    @Put()
    @UsePipes(ValidationPipe)
    update(@Body() dto: UpdateArticleDto): Promise<Article>{
        return this.articleService.update(dto)
    }

    @ApiOperation({summary: 'Delete article by id'})
    @ApiResponse({status: HttpStatus.OK, description: 'Ok', type: Article})
    @ApiResponse({status: HttpStatus.NOT_FOUND, description: 'Not found', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.BAD_REQUEST, description: 'Bad request', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.UNAUTHORIZED, description: 'Unauthorized', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.FORBIDDEN, description: 'Forbidden', type: CustomHttpExceptionResponse})
    @Auth('USER')
    @Delete(':id')
    delete(@Param('id', ParseIntPipe) id: number): Promise<Article>{
        return this.articleService.delete(id)
    }
}
