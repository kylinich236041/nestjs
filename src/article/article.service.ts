import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { response } from 'express';
import { User } from 'src/user/user.entity';
import { UserService } from 'src/user/user.service';
import { DeleteResult, Repository } from 'typeorm';
import { Article } from './article.entity';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';

@Injectable()
export class ArticleService {
    constructor(@InjectRepository(Article) private articleRepository: Repository<Article>) {}

    async create(dto: CreateArticleDto, user: User): Promise<Article>{
        const article = await this.articleRepository.save(dto)
        article.user = user
        await this.articleRepository.save(article)
        return article
    }

    async getAll(): Promise<Article[]>{
        return this.articleRepository.find()
    }

    async getById(id: number): Promise<Article>{
        const article = await this.articleRepository.findOne({where: {id: id}})

        if(!article) throw new HttpException(`Cant find article with id: ${id}`, HttpStatus.NOT_FOUND)

        return article
    }

    async update(dto: UpdateArticleDto): Promise<Article>{
        await this.articleRepository.update({id: dto.id}, {...dto})
        const article = this.getById(dto.id)
        return article
    }

    async delete(id: number): Promise<Article>{
        const article = await this.getById(id)
        await this.articleRepository.delete(article.id)
        return article
    }
}
