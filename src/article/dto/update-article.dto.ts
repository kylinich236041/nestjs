import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, Length } from "class-validator";

export class UpdateArticleDto {
    @ApiProperty({example: '1', description: 'id'})
    @IsNotEmpty()
    readonly id: number;

    @ApiProperty({example: 'About NestJS part 1', description: 'title'})
    @IsNotEmpty()
    @Length(8)
    readonly title: string;

    @ApiProperty({example: 'Introduction\nNest (NestJS) is a framework for building efficient, scalable Node.js server-side applications...', description: 'content'})
    @IsNotEmpty()
    readonly content: string;
}