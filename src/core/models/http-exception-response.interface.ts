import { HttpStatus, RequestMethod } from "@nestjs/common"
import { ApiProperty } from "@nestjs/swagger"

export class HttpExceptionResponse {
    @ApiProperty({example: HttpStatus.FORBIDDEN, description: 'status code'})
    statusCode: number
    @ApiProperty({example: 'Access denied', description: 'message'})
    error: string
}

export class CustomHttpExceptionResponse extends HttpExceptionResponse {
    @ApiProperty({example: '/api/user', description: 'path'})
    path: string
    @ApiProperty({example: 'GET', description: 'method'})
    method: string
    @ApiProperty({example: '2023-09-09T18:46:11.194Z', description: 'date'})
    timeStamp: Date
}