import { HttpException, HttpStatus, NestMiddleware } from "@nestjs/common";
import { NextFunction } from "express";

export class ApiTokenCheckMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: NextFunction) {
        if(req.headers['api-token'] !== 'my-token') throw new HttpException('The token does not match', HttpStatus.BAD_REQUEST)
        next()
    }
}