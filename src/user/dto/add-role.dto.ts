import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class AddRoleDto {
    @ApiProperty({example: 'MODERATOR', description: 'role.value'})
    @IsNotEmpty()
    readonly value: string;

    @ApiProperty({example: '1', description: 'user.id'})
    @IsNotEmpty()
    readonly userId: number;
}