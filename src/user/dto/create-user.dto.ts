import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, Length } from "class-validator";

export class CreateUserDto {
    @ApiProperty({example: 'exampleLogin', description: 'login'})
    @IsNotEmpty({message: 'login should not be empty'})
    @Length(6, 30)
    readonly login: string;

    @ApiProperty({example: 'qwer1234', description: 'password'})
    @IsNotEmpty({message: 'password should not be empty'})
    @Length(8, 128)
    readonly password: string;
}