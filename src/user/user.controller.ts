import { Body, Controller, Get, HttpStatus, Param, ParseIntPipe, Post, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Auth } from 'src/auth/auth.decorator';
import { CustomHttpExceptionResponse } from 'src/core/models/http-exception-response.interface';
import { AddRoleDto } from './dto/add-role.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './user.entity';
import { UserService } from './user.service';

@ApiTags('user')
@Controller('user')
export class UserController {
    constructor(private userService: UserService) {}

    @ApiOperation({summary: 'Create user'})
    @ApiResponse({status: HttpStatus.OK, description: 'Ok', type: User})
    @ApiResponse({status: HttpStatus.BAD_REQUEST, description: 'Bad request', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.UNAUTHORIZED, description: 'Unauthorized', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.FORBIDDEN, description: 'Forbidden', type: CustomHttpExceptionResponse})
    @Auth('ADMIN')
    @Post()
    @UsePipes(ValidationPipe)
    create(@Body() dto: CreateUserDto): Promise<User>{
        return this.userService.create(dto)
    }

    @ApiOperation({summary: 'Get all users'})
    @ApiResponse({status: HttpStatus.OK, description: 'Ok', type: [User]})
    @ApiResponse({status: HttpStatus.UNAUTHORIZED, description: 'Unauthorized', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.FORBIDDEN, description: 'Forbidden', type: CustomHttpExceptionResponse})
    @Auth('ADMIN')
    @Get()
    getAll(): Promise<User[]>{
        return this.userService.getAll()
    }

    @ApiOperation({summary: 'Get user by id'})
    @ApiResponse({status: HttpStatus.OK, description: 'Ok', type: User})
    @ApiResponse({status: HttpStatus.NOT_FOUND, description: 'Not found', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.UNAUTHORIZED, description: 'Unauthorized', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.FORBIDDEN, description: 'Forbidden', type: CustomHttpExceptionResponse})
    @Auth('ADMIN')
    @Get(':id')
    getById(@Param('id', ParseIntPipe) id: number): Promise<User>{
        return this.userService.getById(id)
    }
    
    @ApiOperation({summary: 'Add user role'})
    @ApiResponse({status: HttpStatus.OK, description: 'Ok', type: User})
    @ApiResponse({status: HttpStatus.NOT_FOUND, description: 'Not found', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.UNAUTHORIZED, description: 'Unauthorized', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.FORBIDDEN, description: 'Forbidden', type: CustomHttpExceptionResponse})
    @Auth('ADMIN')
    @Post('role/add')
    addRole(@Body() dto: AddRoleDto): Promise<User>{
        return this.userService.addRole(dto)
    }

    @ApiOperation({summary: 'Remove user role'})
    @ApiResponse({status: HttpStatus.OK, description: 'Ok', type: User})
    @ApiResponse({status: HttpStatus.NOT_FOUND, description: 'Not found', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.UNAUTHORIZED, description: 'Unauthorized', type: CustomHttpExceptionResponse})
    @ApiResponse({status: HttpStatus.FORBIDDEN, description: 'Forbidden', type: CustomHttpExceptionResponse})
    @Auth('ADMIN')
    @Post('role/remove')
    removeRole(@Body() dto: AddRoleDto): Promise<User>{
        return this.userService.removeRole(dto)
    }
}
