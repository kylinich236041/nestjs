import { ApiProperty } from "@nestjs/swagger";
import { Article } from "src/article/article.entity";
import { Role } from "src/role/role.entity";
import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User {
    @ApiProperty({example: '1', description: 'unique identificator'})
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty({example: 'exampleLogin', description: 'login'})
    @Column()
    login: string;

    @ApiProperty({example: 'qwer1234', description: 'password'})
    @Column()
    password: string;

    @ApiProperty({example: [Role], description: 'array of roles'})
    @ManyToMany(() => Role, role => role.users)
    @JoinTable()
    roles: Role[]

    @ApiProperty({example: [Article], description: 'array of articles'})
    @OneToMany(() => Article, article => article.user)
    articles: Article[]
}