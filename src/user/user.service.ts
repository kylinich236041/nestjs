import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RoleService } from 'src/role/role.service';
import { Repository } from 'typeorm';
import { AddRoleDto } from './dto/add-role.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './user.entity';

@Injectable()
export class UserService {
    constructor(@InjectRepository(User) private userRepository: Repository<User>,
                private roleService: RoleService) {}

    async create(dto: CreateUserDto): Promise<User>{
        return await this.userRepository.save(dto)
    }

    async getAll(): Promise<User[]>{
        return this.userRepository.find({relations: ['roles', 'articles']})
    }

    async getById(id: number): Promise<User>{
        const user = await this.userRepository.findOne({where: {id: id}, relations: ['roles', 'articles']})

        if(!user) throw new HttpException(`Cant find user with id: ${id}`, HttpStatus.NOT_FOUND)

        return user
    }

    async getByLogin(login: string){
        return await this.userRepository.findOne({where: {login: login}, relations: ['roles']})
    }

    async addRole(dto: AddRoleDto): Promise<User>{
        const user = await this.userRepository.findOne({where: {id: dto.userId}, relations: ['roles']})
        if(!user) throw new HttpException(`Cant find user with id: ${dto.userId}`, HttpStatus.NOT_FOUND)
        
        const role = await this.roleService.getByValue(dto.value)
        
        user.roles.push(role)
        await this.userRepository.save(user)

        return user
    }

    async removeRole(dto: AddRoleDto): Promise<User>{
        const user = await this.userRepository.findOne({where: {id: dto.userId}, relations: ['roles']})
        if(!user) throw new HttpException(`Cant find user with id: ${dto.userId}`, HttpStatus.NOT_FOUND)

        const role = await this.roleService.getByValue(dto.value)
        
        user.roles = user.roles.filter(r => r.value !== role.value)
        await this.userRepository.save(user)

        return user
    }
}
