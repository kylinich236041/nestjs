import { TypeOrmModuleOptions } from "@nestjs/typeorm";

export const typeOrmConfig: TypeOrmModuleOptions = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'root',
    database: 'nestjs2022',
    entities: [
        __dirname + '/../**/*.entity{.ts,.js}',
    ],
    synchronize: true,
    // type: 'postgres',
    // host: process.env.POSTGRES_HOST,
    // port: Number(process.env.POSTGRES_PORT),
    // username: process.env.POSTGRES_USER,
    // password: process.env.POSTGRES_PASSWORD,
    // database: process.env.POSTGRES_DB,
    // entities: [
    //     __dirname + '/../**/*.entity{.ts,.js}',
    // ],
    // synchronize: true,
}