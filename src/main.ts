import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

import * as morgan from "morgan";
import * as fs from "fs";

const logStream = fs.createWriteStream('api.log', {
  flags: 'a'
})

async function bootstrap() {
  const PORT = process.env.PORT
  const app = await NestFactory.create(AppModule);

  app.setGlobalPrefix('/api')
  app.enableCors()
  app.use(morgan('combined', { stream: logStream }))

  const config = new DocumentBuilder()
    .setTitle('Example REST API')
    .setDescription('This project was created for educational purposes by Yan Kulinych')
    .setVersion('1.0.0')
    .build()
  const document = SwaggerModule.createDocument(app, config)
  SwaggerModule.setup('api', app, document)

  await app.listen(PORT, () => console.log(`Server started on PORT: ${PORT}`));
}

bootstrap();